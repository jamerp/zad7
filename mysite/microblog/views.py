from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.contrib.auth.models import User

from models import Entry

def logout_view(request):
	c = {}
	c.update(csrf(request))

	logout(request)
	c['username'] = ''
	c['message'] = 'You are correctly logout!'

	return render_to_response('microblog/login.html', c)

def login_view(request):
	c = {}
	c.update(csrf(request))

	if request.user.is_authenticated():
		c['username'] = request.user.username
		return render_to_response('microblog/login.html', c)


	c['username'] = ''
	if request.method == 'POST':
		username = request.POST.get('username', '')
		password = request.POST.get('password', '')
		user = authenticate(username=username, password=password)
		if user is not None:
			login(request, user)
			c['username'] = username
		else:
			c['message'] = 'Incorrect login or password!'

	return render_to_response('microblog/login.html', c)

def fulllist(request):
	dict = {
		'entries' : Entry.objects.order_by('-date'),
		'username' : request.user.username if request.user.is_authenticated() else '',
		'users' : User.objects.all()
	}
	return render_to_response('microblog/list.html', dict)

def list(request, username):
	dict = {
		'entries' : Entry.objects.filter(author=username).order_by('-date'),
		'username' : request.user.username if request.user.is_authenticated() else '',
		'users' : User.objects.all()
	}
	return render_to_response('microblog/list.html', dict)

def form(request):
	c = {}
	c.update(csrf(request))

	if not request.user.is_authenticated():
		c['username'] = ''
		return render_to_response('microblog/login.html', c)

	c['username'] = request.user.username
	if request.method == "POST":
		Entry(
			title=request.POST.get('title', ''),
			content=request.POST.get('content', ''),
			date=timezone.now(),
			author=request.user.username
		).save()
		c['entries'] = Entry.objects.order_by('-date')
		c['users'] = User.objects.all()
		return render_to_response('microblog/list.html', c)

	return render_to_response('microblog/form.html', c)
