from django.contrib import admin
from microblog.models import Entry, User

admin.site.register(Entry)
admin.site.register(User)
