from django.conf.urls import patterns, url
from django.views.generic import ListView
from microblog.models import Entry

import views

urlpatterns = patterns('',
	url(r'^list$', views.fulllist, name='full_list'),
	url(r'^list/(?P<username>.*)/$', views.list, name='list'),
	url(r'^add$', views.form, name='form'),
	url(r'^login$', views.login_view, name='login'),
	url(r'^logout$', views.logout_view, name='logout'),
)
