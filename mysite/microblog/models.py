from django.db import models

class Entry(models.Model):
	title = models.CharField(max_length=500)
	author = models.CharField(max_length=100)
	content = models.CharField(max_length=2000)
	date = models.DateTimeField()

	def __unicode__(self):
		return self.title

class User(models.Model):
	username = models.CharField(max_length=100)
	password = models.CharField(max_length=100)

	def __unicode__(self):
		return self.username
